import { createAsyncThunk } from "@reduxjs/toolkit";

import { evaluateService } from "../../../services/ship";

import { setGoToError, setOpenSpinner } from "../App/actions";

import {
  homeSlice,
} from "./reducer";

const {
  setResult,
} = homeSlice.actions;

const evaluateAction = createAsyncThunk<any, string, null>(
  "home/evaluate",
  async (params, thunk) => {
    thunk.dispatch(setOpenSpinner(true));

    try {
      const response = await evaluateService(params);

      thunk.dispatch(setOpenSpinner(false));
      return response;
    } catch (error) {
      thunk.dispatch(setGoToError(true));
      thunk.dispatch(setOpenSpinner(false));
      return "";
    }
  }
);

export {
  evaluateAction,
  setResult,
};
