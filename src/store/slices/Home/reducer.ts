import { createSlice } from "@reduxjs/toolkit";

import {
  evaluateAction
} from "./actions";

const initialState: THomeReducer = {
  result: "",
};

export const homeSlice = createSlice({
  name: "home",
  initialState,
  reducers: {
    setResult: (state, action) => {
      state.result = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(evaluateAction.fulfilled, (state, action) => {
      state.result = (action.payload);
    })
  },
});
