import { configureStore } from "@reduxjs/toolkit";

import { appSlice } from "./slices/App/reducer";
import { homeSlice } from "./slices/Home/reducer";

const store = configureStore({
  devTools: process.env.NODE_ENV === "development" ? true : false,
  reducer: {
    app: appSlice.reducer,
    home: homeSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
