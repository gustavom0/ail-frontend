export default {
  plusJakartaSansRegular: "PlusJakartaSans",
  plusJakartaSansBoldFamily: "PlusJakartaSans Bold",
  plusJakartaSansSemiBoldFamily: "PlusJakartaSans SemiBold",
  plusJakartaSansExtraBoldFamily: "PlusJakartaSans ExtraBold",
  plusJakartaSansLightFamily: "PlusJakartaSans Light",
  plusJakartaSansExtraLightFamily: "PlusJakartaSans ExtraLight",
  plusJakartaSansMedium: "PlusJakartaSans Medium",
};
