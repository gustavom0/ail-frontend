import { Route, Routes } from "react-router-dom";

import ErrorPage from "../pages/Error";
import HomePage from "../pages/Home";
import NotFoundPage from "../pages/404";

export const Routing = () => {
  return (
    <Routes>
      <Route path={"/"} element={<HomePage />} />
      <Route path={"/error"} element={<ErrorPage />} />
      <Route path={"*"} element={<NotFoundPage />} />
    </Routes>
  );
};
