import { HeaderContainer } from "./style";

import { Title } from "@tranqi/ui-kit";

const Header = () => {
  return (
    <HeaderContainer>
      <Title size={"small"}>
        {"Artemisa 1"}
      </Title>
    </HeaderContainer>
  );
};

export default Header;
