import executeRequest from "./index";

export const evaluateService: TEvaluateService = async (params) => {
  try {
    const response = await executeRequest("/ship/evaluate", JSON.parse(params), "POST");

    return response;
  } catch (error) {
    console.log('error', error);
    throw error;
  }
};
