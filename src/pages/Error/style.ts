import styled from "styled-components";

const Container = styled.main`
`;

const Content = styled.section`
  margin: auto;
  width: calc(100% - 260px);
`;

export {
  Container,
  Content,
};
