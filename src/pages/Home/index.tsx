import { Controller, FieldError, useForm } from "react-hook-form";

import {
  Block,
  Button,
  ButtonsContainer,
  Text,
  Textarea,
} from "@tranqi/ui-kit";

import MainContainer from "../../components/MainContainer";
import SectionContent from "../../components/SectionContent";

import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { evaluateAction } from "../../store/slices/Home/actions";

const HomePage = () => {
  const dispatch = useAppDispatch();
  const { result } = useAppSelector((state) => state.home);

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const getMessageError = (fieldError: FieldError) => {
    if (fieldError) {
      if (fieldError.type === "required") {
        return "Este campo es obligatorio";
      }
      else {
        return fieldError.message;
      }
    }
  };

  const getFormDisabled = () => {
    if (Object.keys(errors).length) {
      return true;
    }

    return false;
  };

  const onSubmit = handleSubmit((data) => {
    dispatch(evaluateAction(data.input));
  });

  return (
    <MainContainer>
      <SectionContent>
        <Block space={3}>
          <Text size={"small"}>{"Expresión a evaluar"}</Text>
        </Block>
        <form
          onSubmit={onSubmit}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              e.preventDefault();
            }
          }}
        >
          <Block space={3}>
            {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
            {/* @ts-ignore */}
            <Controller
              control={control}
              name={"input"}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <Textarea
                  {...field}
                  placeholder={""}
                  name={"input"}
                  error={getMessageError(errors.input as FieldError)}
                />
              )}
            />
          </Block>
          <Block space={3}>
            <ButtonsContainer>
              <Button
                disabled={getFormDisabled()}
                type={"submit"}
                variant={"fill"}
                width={165}
              >
                {"Evaluar"}
              </Button>
            </ButtonsContainer>
          </Block>
          <Block space={3}>
            <Text size={"small"}>{"Resultado de la operación"}</Text>
          </Block>
          <Block space={3}>
            <Textarea
              name={"result"}
              placeholder={""}
              value={JSON.stringify(result)}
            />
          </Block>
        </form>
      </SectionContent>
    </MainContainer>
  );
};

export default HomePage;
