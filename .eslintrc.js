/* eslint-disable no-undef */
module.exports = {
  parser: "@typescript-eslint/parser",
  root: true,
  env: {
    browser: true,
    es2021: true,
  },
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  plugins: [
    "react",
    "@typescript-eslint",
  ],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
  ],
  rules: {
    quotes: ["warn", "double"],
    "sort-imports": ["warn", { allowSeparatedGroups: true }],
    "react/react-in-jsx-scope": "off",
    "no-useless-catch": "off",
  },
};
